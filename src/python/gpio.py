import time, sys
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
LED_PIN = 18
GPIO.setup(LED_PIN, GPIO.OUT)


def logit(message):
    print(message)
    sys.stdout.flush()


def blink():
    logit("high")
    GPIO.output(LED_PIN, GPIO.HIGH)
    time.sleep(2)
    logit("low")
    GPIO.output(LED_PIN, GPIO.LOW)


if __name__ == '__main__':
    try:
        while True:
            time.sleep(2)
            blink()
    except:
        logit("Bye!")
        GPIO.cleanup()
const Redis = require('ioredis');

const redis = new Redis(6379, "redis"); //"redis" is the docker instance name

redis.on('message', (channel, message) => {
    console.log(`Received the following message from ${channel}: ${message}`);
});

const channel = 'DAVID_TEST_1';

redis.subscribe(channel, (error, count) => {
    if (error) {
        throw new Error(error);
    }
    console.log(`Subscribed to ${count} channel. Listening for updates on the ${channel} channel.`);
});
#rpi-docker-gpio

### Dockerize
Build and push:
```
$ docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v7 -t davidjbarnes/rpi-test --push .
```

Run:
```
$ docker run --name rpi-test -d --rm  --link redis-server:redis davidjbarnes/rpi-test
```

```
$ docker run --device /dev/gpiomem --privileged -i --rm --name rpi davidjbarnes/rpi-test
```

Access redis-cli on Docker redis-server container:
```
$ docker exec -it redis-server redis-cli
```


* Ensure redis is running on default port 6379